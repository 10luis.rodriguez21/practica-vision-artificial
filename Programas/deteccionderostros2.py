import cv2
import face_recognition
import numpy as np


#DETECCION DE CAMARA
CAMARA=cv2.VideoCapture(0,cv2.CAP_DSHOW)

# Almacenando una 'codificación' universal de los rasgos faciales de Abinader 
Foto_Abinader = face_recognition.load_image_file("abinader.jpg")
Foto_Abinader_Reconocida = face_recognition.face_encodings(Foto_Abinader)[0]

# Almacenando una 'codificación' universal de los rasgos faciales de Danilo Media
Foto_Danilo = face_recognition.load_image_file("danilo.jpg")
Foto_Danilo_Reconocida = face_recognition.face_encodings(Foto_Danilo)[0]

Codificaciones_Rasgos_Faciales = [
    Foto_Abinader_Reconocida,
    Foto_Danilo_Reconocida
]
Matriz_Nombres_Caras_Conocidas = [
    "Luis Abinader",
    "Danilo Medina"
]
while True:
   
    valido, cuadro= CAMARA.read()
    rgb_cuadro = cuadro[:, :, ::-1]

    Ubicacion_rostros = face_recognition.face_locations(rgb_cuadro)
    Codificaciones_rostros = face_recognition.face_encodings(rgb_cuadro, Ubicacion_rostros)

    for (top, right, bottom, left), Rostro in zip(Ubicacion_rostros, Codificaciones_rostros):

        
        Compatibilidad = face_recognition.compare_faces(Codificaciones_Rasgos_Faciales, Rostro)

        nombre = "Desconocido"

        Distancia_Rostros = face_recognition.face_distance(Codificaciones_Rasgos_Faciales, Rostro)
        Mejor_Coincidencia_Index = np.argmin(Distancia_Rostros)
        if Compatibilidad[Mejor_Coincidencia_Index]:
            nombre = Matriz_Nombres_Caras_Conocidas[Mejor_Coincidencia_Index]

        cv2.rectangle(cuadro, (left, top), (right, bottom), (0, 255, 255), 2)

        cv2.rectangle(cuadro, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_ITALIC
        cv2.putText(cuadro, nombre, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    
    cv2.imshow("Camara", cuadro)
    if cv2.waitKey(1)==ord('s'):
        break

CAMARA.release()
cv2.destroyAllWindows()