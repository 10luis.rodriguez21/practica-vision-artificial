import cv2
import pytesseract
pytesseract.pytesseract.tesseract_cmd ='C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
imagen = cv2.imread("texto2.jpg")
imagenreajustada = cv2.resize(imagen, (400, 400))
cv2.imshow("Imagen con el texto", imagenreajustada)
texto = pytesseract.image_to_string(imagenreajustada)
print(texto)
cv2.waitKey(0)
cv2.destroyAllWindows()